package ru.dreendex.translationshelper;

import ru.dreendex.translationshelper.lang.LangManager;
import ru.dreendex.translationshelper.lang.Message;

import javax.swing.*;
import javax.swing.tree.DefaultTreeCellRenderer;
import java.awt.*;
import java.net.URL;
import java.util.HashMap;

public class TranslationsTreeRenderer extends DefaultTreeCellRenderer {

    private Icon folderIcon = createImageIcon("/folder.png");
    private Icon openFolderIcon = createImageIcon("/folder_open.png");
    private Icon translationFolderIcon = createImageIcon("/folder_with_translation.png");
    private Icon translationIcon = createImageIcon("/translation.png");
    private Icon multilineTranslationIcon = createImageIcon("/multiline_translation.png");
    private HashMap<String, Icon> countryIcons = new HashMap<>();
    private LangManager langs;

    public TranslationsTreeRenderer(LangManager langs) {
        this.langs = langs;
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        //https://www.countryflags.io/ru/flat/16.png
        Icon icon = folderIcon;
        this.setTextSelectionColor(Color.BLACK);
        this.setTextNonSelectionColor(Color.BLACK);
        if (value instanceof LangTreeNode) {
            LangTreeNode node = (LangTreeNode) value;
            Message message = node.getMessage();
            if (message != null) {
                if (!message.hasLang(node.getLang())) {
                    this.setTextSelectionColor(Color.RED);
                    this.setTextNonSelectionColor(Color.RED);
                }
                icon = message.getDefaultTranslation().contains("\n") ? multilineTranslationIcon : translationIcon;
            } else if (node.getLang().equals(node.toString())) {
                icon = getCountryIcon(value.toString());
            }
        }
        this.setLeafIcon(icon);
        this.setOpenIcon(icon == folderIcon ? openFolderIcon : icon);
        this.setClosedIcon(icon);
        this.setToolTipText("Needs translation");
        return super.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);
    }

    private Icon getCountryIcon(String countryCode) {
        return countryIcons.computeIfAbsent(countryCode, k -> {
            Icon icon = createImageIcon("/flag_" + countryCode + ".png");
            if (icon == null) {
                icon = folderIcon;
            }
            return icon;
        });
    }

    public static ImageIcon createImageIcon(String path) {
        URL imgURL = TranslationsTreeRenderer.class.getResource(path);
        if (imgURL != null) {
            return new ImageIcon(imgURL);
        } else {
            System.err.println("Couldn't find file: " + path);
            return null;
        }
    }
}
