package ru.dreendex.translationshelper.lang;

import com.alee.managers.log.Log;
import com.google.gson.*;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class LangManager {

    private Set<String> languages;
    private HashMap<String, Message> messages; // key, message

    public LangManager() {
        this.languages = new HashSet<>();
        this.messages = new HashMap<>();
    }

    public void loadLanguages(File directory) {
        File[] dirs = directory.listFiles();
        if (dirs != null) {
            for (File dir : dirs) {
                if (dir.isDirectory() && !dir.getName().startsWith(".")) {
                    File[] files = dir.listFiles();
                    if (files != null) {
                        for (File file : files) {
                            if (file.isFile() && file.getName().endsWith(".json") && !file.getName().equals("minecraft.json")) {
                                loadLangFile(dir.getName(), file);
                            }
                        }
                    }
                }
            }
        }
    }

    private void loadLangFile(String lang, File file) {
        try {
            JsonObject json = new JsonParser().parse(FileUtils.readFileToString(file, StandardCharsets.UTF_8)).getAsJsonObject();
            for (Map.Entry<String, JsonElement> entry : json.entrySet()) {
                if (entry.getValue().isJsonPrimitive() && entry.getValue().getAsJsonPrimitive().isString()) {
                    languages.add(lang);
                    messages.computeIfAbsent(entry.getKey(), k -> new Message(entry.getKey()))
                            .putTranslation(lang, entry.getValue().getAsJsonPrimitive().getAsString());
                }
            }
        } catch (Exception e) {
            Log.error("Error loading '" + file.getName() + "' file in '" + lang + "' lang");
            e.printStackTrace();
        }
    }

    public Set<String> getLanguages() {
        return languages;
    }

    public Message getMessage(String key) {
        return messages.get(key);
    }

    public Collection<Message> getMessages() {
        return messages.values();
    }

    public void setAndSave(File workingDir, String lang, Message message, String text) {
        message.putTranslation(lang, text);

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        File dir = new File(workingDir, lang);
        if (!dir.exists()) {
            dir.mkdir();
        }

        JsonObject json = new JsonObject();
        String fileName = message.getKey().split("\\.")[0];

        getMessages().stream()
                .filter(msg -> msg.getKey().startsWith(fileName))
                .sorted(Comparator.naturalOrder())
                .forEach(msg -> json.addProperty(msg.getKey(), msg.getTranslation(lang)));

        try {
            FileUtils.writeStringToFile(new File(dir, fileName + ".json"), gson.toJson(json) + "\n", StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
