package ru.dreendex.translationshelper;

import com.alee.laf.WebLookAndFeel;
import com.alee.managers.log.Log;

import javax.swing.*;

public class Form {

    private JPanel contentPanel;
    private JTextArea defaultTranslation;
    private JTextArea currentTranslation;
    private JTree tree;
    private JSplitPane splitPane;
    private JPanel panel1;
    private JToolBar toolbar;
    private JButton saveButton;
    private JLabel statusLabel;

    public Form() {

    }

    public JPanel getContentPanel() {
        return contentPanel;
    }

    public JTextArea getDefaultTranslation() {
        return defaultTranslation;
    }

    public JTextArea getCurrentTranslation() {
        return currentTranslation;
    }

    public JTree getTree() {
        return tree;
    }

    public JToolBar getToolbar() {
        return toolbar;
    }

    public JButton getSaveButton() {
        return saveButton;
    }

    public JLabel getStatusLabel() {
        return statusLabel;
    }

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(new WebLookAndFeel());
        } catch (UnsupportedLookAndFeelException e) {
            Log.error("Unable to setup beauty");
            e.printStackTrace();
        }
        new TranslatorHelper().start();
    }
}
